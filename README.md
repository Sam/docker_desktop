# Docker Desktop

--------------------------------------------------------------------------

For 3 months, I was stuck using a "managed" corporate laptop at a contracting gig. The company was quite large and dysfunctional (I won't mention their name). They provided me with none of the tools I needed to do my job or any means to install them, but I did have Docker pre-installed. I ended up doing most of my local desktop work from a Docker container. When all you have is a hammer, everything looks like a nail.

--------------------------------------------------------------------------