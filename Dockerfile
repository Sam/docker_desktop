FROM homebrew/brew

# Standard encoding
RUN echo 'LC_ALL=en_US.UTF-8' >> /etc/environment
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
RUN echo 'LANG=en_US.UTF-8' > /etc/locale.conf
RUN locale-gen en_US.UTF-8
RUN mkdir -p /usr/src/app

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -yq \
    && apt-get dist-upgrade -yq \
    # Fallback base pkgs
    && apt-get install make \
                       build-essential \
                       libssl-dev \
                       zlib1g-dev \
                       libbz2-dev \
                       libreadline-dev \
                       libsqlite3-dev \
                       libncurses5-dev \
                       libncursesw5-dev \
                       xz-utils \
                       tk-dev \
                       libffi-dev \
                       liblzma-dev \
                       python-openssl \
                       fonts-powerline \
                       fonts-hack-ttf \
                       pkg-config \
                       libssl-dev \
                       libreadline5 \
                       libreadline-dev \
                       bzip2 \
                       lbzip2 \
                       libsqlite0 \
                       squashfuse \
                       sqlite3 -yq


# Homebrew prime
RUN for tap in cjbassi/gotop \
               the-other-sam/extras \
               linuxbrew/xorg ; \ 
      do brew tap "$tap"; done

RUN brew update \
    && brew upgrade \
    && brew cleanup -s \
    && brew install llvm


# Homebrew Config
ENV LD_LIBRARY_PATH="/home/linuxbrew/.linuxbrew/lib:$LD_LIBRARY_PATH"
ENV C_LIBRARY_PATH="/home/linuxbrew/.linuxbrew/include:$C_LIBRARY_PATH"
ENV PKG_CONFIG_PATH="/home/linuxbrew/.linuxbrew/opt/pkgconfig"
ENV NVM_DIR="$HOME/.nvm"
ENV HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew"


# Homebrew Main Environment
RUN brew install    asdf \
                    nim \
                    git \
                    mercurial \
                    svn \
                    rust \
                    golang \
                    micro \
                    neovim \
                    vim \
                    nano \
                    bash \
                    fish \
                    zsh  \
                    with-readline \
                    luarocks \
                    diskus \
                    dust \
                    wget \
                    curl \
                    rsync \
                    pyenv \
                    rbenv \
                    ack \
                    fd \
                    exa \
                    bat \
                    ccat \
                    fzf \
                    diff-so-fancy \
                    less \
                    moreutils \
                    flex \
                    nnn \
                    ncdu \
                    ripgrep \
                    jq \
                    stemplate \
                    stoml \
                    gotop \
                    typescript \
                    nvm \
                    pyoxidizer \
                    rustc-completion \
                    bash-completion \
                    bundler-completion \
                    cargo-completion \
                    fabric-completion \
                    gem-completion \
                    pip-completion \
                    rake-completion \
                    ruby-completion \
                    zsh-completion \
                    zsh-syntax-highlighting \
                    zsh-autosuggestions \
                    zsh-completions \
                    zsh-git-prompt \
                    zsh-history-substring-search


#Shell config
RUN mkdir ~/.nvm
RUN ln -s /home/linuxbrew/.linuxbrew/bin/bash /usr/local/bin/bash
RUN ln -s /home/linuxbrew/.linuxbrew/bin/fish /usr/local/bin/fish
RUN ln -s /home/linuxbrew/.linuxbrew/bin/zsh /usr/local/bin/zsh
RUN echo '/usr/local/bin/bash' >> /etc/shells
RUN echo '/usr/local/bin/fish' >> /etc/shells
RUN echo '/usr/local/bin/zsh' >> /etc/shells
RUN chsh -s /usr/local/bin/bash root
ENV TERM=xterm-256color
ENV EDITOR=micro
SHELL ["/usr/local/bin/bash", "-c"]

#Bash config
RUN /usr/local/bin/bash -c 'git clone --depth=1 https://github.com/Bash-it/bash-it.git \
            ~/.bash_it' && ~/.bash_it/install.sh --silent 

RUN echo 'bash_it enable plugin history \
                                git \
                                go \
                                node \
                                nvm \
                                pyenv \
                                rbenv \
                                ruby \
                                python' | /usr/local/bin/bash -i &2>/dev/null

RUN echo 'bash_it enable completion brew \
                                    git \
                                    go \
                                    gem \
                                    gulp \
                                    nvm \
                                    npm \
                                    pip' | /usr/local/bin/bash -i &2>/dev/null

RUN echo '  [ -s "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh" ] \
    && . "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh"' >> ~/.bashrc
RUN echo '  [ -s "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion.d/nvm" ] \
    && . "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion.d/nvm"' >> ~/.bashrc

RUN cat ~/.bashrc.bak >> ~/.bashrc && rm ~/.bashrc.bak

RUN /usr/local/bin/bash &>/dev/null


#Zsh config
ENV ZSH_DISABLE_COMPFIX=true
ENV ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=cyan,bold,underline"

RUN mkdir -p ~/.local/share/fonts \
    && cd ~/.local/share/fonts \
    && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf

RUN mkdir ~/.antigen
RUN curl -L git.io/antigen > ~/.antigen/antigen.zsh
RUN git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
RUN echo 'source ~/.antigen/antigen.zsh' >> ~/.zshrc
RUN echo 'antigen use oh-my-zsh' >> ~/.zshrc
RUN echo 'antigen bundle git \
                         brew \
                         autojump \
                         battery \
                         command-not-found \
                         common-aliases \
                         compleat \
                         git-extras \
                         z \
                         pip \
                         mattberther/zsh-pyenv \
                         rbenv \
                         virtualenv \
                         zsh-nvm \
                         npm \
                         zsh-users/zsh-completions \
                         zsh-users/zsh-syntax-highlighting \
                         zsh-users/zsh-autosuggestions' >> ~/.zshrc
RUN echo 'antigen theme romkatv/powerlevel10k' >> ~/.zshrc
RUN echo 'POWERLEVEL9K_MODE='nerdfont-complete'' >> /.zshrc
RUN echo 'fpath=(/home/linuxbrew/.linuxbrew/share/zsh-completions $fpath)' >> /.zshrc
RUN echo 'source /home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh' >> ~/.zshrc
RUN echo 'source /home/linuxbrew/.linuxbrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh' >> ~/.zshrc
RUN echo 'source /home/linuxbrew/.linuxbrew/share/zsh-history-substring-search/zsh-history-substring-search.zsh' >> ~/.zshrc
RUN echo 'source /home/linuxbrew/.linuxbrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> ~/.zshrc 
RUN echo 'autoload predict-on' >> ~/.zshrc
RUN chown -R root:linuxbrew /home/linuxbrew/.linuxbrew/Homebrew/completions/zsh/*
RUN echo 'antigen apply &>/dev/null' >> ~/.zshrc
RUN echo 'eval "$(rbenv init -)"' >> ~/.zshenv
RUN echo 'eval "$(pyenv init -)"' >> ~/.zshenv
WORKDIR /root/
ADD .p10k.zsh .
RUN echo 'source ~/.p10k.zsh' >> ~/.zshrc

RUN zsh &>/dev/null
 

#Fish config
RUN mkdir -p ~/.config/fish
RUN echo 'set -U fish_greeting ""' >> ~/.config/fish/config.fish

RUN curl -L https://get.oh-my.fish > install \
    && fish install --noninteractive \
    && rm install

RUN echo 'omf install fonts \
                      foreign-env \
                      pyenv \
                      nvm \
                      rbenv \
                      autovenv \
                      python \
                      spacefish \
                      && omf reload' | fish 2>&1 >/dev/null

RUN fish &>/dev/null


#Ruby config
RUN rm -rf /home/linuxbrew/.linuxbrew/bin/bundle
RUN echo 'set -x LTS_RUBY (rbenv install --list | grep -v [evr] | sort -r | head -n 1 | xargs) \
         && rbenv install $LTS_RUBY \
         && rbenv global $LTS_RUBY \
         && set -e LTS_RUBY' \
         | fish 2>&1 >/dev/null

RUN echo '# add rbenv & pyenv support' >> ~/.bashrc
RUN echo 'rbenv init - &>/dev/null' >> ~/.bashrc
RUN echo 'pyenv init - &>/dev/null' >> ~/.bashrc

RUN fish && echo "y" | gem update --force --no-document --silent \
         && gem install awesome_print \
                        clamp \
                        darkfish-rdoc \
                        rdoc \
                        formatador \
                        fpm \ 
                        pry \
                        rake \
                        rspec \
                        thor \
                        commander 2>&1 >/dev/null


#Python config
ENV PYTHON_CONFIGURE_OPTS=--enable-unicode=ucs2

RUN echo 'set -x LTS_PYTHON (pyenv install --list | grep -v [adprsy] | sort -r | head -n 1 | xargs) \
         && pyenv install $LTS_PYTHON \
         && pyenv global $LTS_PYTHON \
         && set -e LTS_PYTHON' \
         | fish 2>&1 >/dev/null
         
RUN echo 'pip install --upgrade pip \
         setuptools \
         wheel \ 
         && pip install pip-review \
         && pip-review -a' | fish 2>&1 >/dev/null
         
RUN echo 'pip install yq \
                      pip-run \
                      pex \
                      pydf \
                      shiv \
                      boto \
                      meson \
                      virtualenv \
                      xar \
                      flask \
                      fabric \
                      ansible' | fish 2>&1 >/dev/null


#Node config
RUN echo 'nvm install --lts \
         && npm update -g npm \
         && npm install --silent -g gulp \
                                    react \
                                    koa \
                                    webpack \
                                    nexe' | fish 2>&1 >/dev/null



#Starlark DSL 
RUN export GOPATH=/opt && go get -u go.starlark.net/cmd/starlark \
                       && mv /opt/bin/starlark /usr/local/bin/ \
                       && rm -rf /opt/src \
                       && rmdir /opt/bin

#Default startup
WORKDIR /usr/src/app
ENTRYPOINT ["/usr/local/bin/bash", "-c"]
CMD ["/usr/local/bin/zsh"]
